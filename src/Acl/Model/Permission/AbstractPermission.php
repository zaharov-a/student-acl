<?php


namespace Ox3a\Acl\Model\Permission;

use JsonSerializable;
use Ox3a\Acl\Model\Acl;

/**
 * Class AbstractPermission
 * @package Ox3a\Acl\Model\Permission
 */
abstract class AbstractPermission implements JsonSerializable
{
    /**
     * @var mixed
     */
    protected $_value;

    protected $_isBranch = false;

    protected $_id    = 0;
    protected $_path  = [];
    protected $_type  = 0;
    protected $_title = "";
    protected $_factory;


    abstract public function setValue($value);


    /**
     * AbstractPermission constructor.
     * @param Acl   $factory
     * @param array $options
     */
    public function __construct($factory, $options)
    {
        $this->_factory = $factory;
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if ($key != 'value' && !is_null($value) && method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        if (isset($options['value'])) {
            $this->setValue($options['value']);
        }
    }


    /**
     * @param int|mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }


    /**
     * @param array|mixed $path
     */
    public function setPath($path)
    {
        $this->_path = $path;
    }


    /**
     * @param mixed|string $title
     */
    public function setTitle($title)
    {
        $this->_title = $title;
    }


    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->_type = $type;
    }


    public function isBranch()
    {
        return $this->_isBranch;
    }


    public function getValue()
    {
        return $this->_value;
    }


    public function getId()
    {
        return $this->_id;
    }


    public function getPath()
    {
        return $this->_path;
    }


    public function getTitle()
    {
        return $this->_title;
    }


    /**
     * @return int
     */
    public function getType()
    {
        return $this->_type;
    }


    public function jsonSerialize()
    {
        return $this->_value;
    }


}
