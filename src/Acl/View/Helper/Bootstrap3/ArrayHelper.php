<?php


namespace Ox3a\Acl\View\Helper\Bootstrap3;


use Ox3a\Acl\Model\Permission\ArrayPermission;

class ArrayHelper extends AbstractHelper
{

    public function render()
    {
        /** @var ArrayPermission $permission */
        $permission = $this->_model;
        $name       = $this->getName();
        $control    = '';
        foreach ($permission->getOptions() as $value => $text) {
            $control .= sprintf('<option value="%s">%s</option>', $value, $text);
        }
        $control = sprintf(
            '<select class="form-control input-sm" name="%s" size="5" multiple="multiple">%s</select>',
            $name,
            $control
        );
        return sprintf(
            '<tr><td>%s</td><td>%s</td></tr>',
            $permission->getTitle(),
            $control
        );
    }

}
