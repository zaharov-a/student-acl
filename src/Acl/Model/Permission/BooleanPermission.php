<?php


namespace Ox3a\Acl\Model\Permission;

/**
 * Class BooleanPermission
 * Логическое право: да, нет
 * @package Ox3a\Acl\Model\Permission
 */
class BooleanPermission extends AbstractPermission
{
    protected $_value = false;


    public function setValue($value)
    {
        $this->_value = (bool)$value;
    }


    public function isTrue()
    {
        return $this->_value;
    }


    public function isAllowed()
    {
        return $this->isTrue();
    }


    public function isFalse()
    {
        return !$this->isTrue();
    }


    public function isDenied()
    {
        return $this->isFalse();
    }
}
