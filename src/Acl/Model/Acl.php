<?php


namespace Ox3a\Acl\Model;


use Ox3a\Acl\Model\Permission\PermissionNotFoundException;

class Acl
{
    protected $_hashTable = [];

    protected $_root;

    protected $_classes = [
        Permission\BranchPermission::class,
        Permission\BooleanPermission::class,
        Permission\AccessPermission::class,
        Permission\ArrayPermission::class,
    ];


    public function __construct($tree = null, $classMap = null)
    {
        if ($classMap) {
            foreach ($classMap as $id => $class) {
                $this->assign($id, $class);
            }
        }

        if ($tree) {
            $this->loadTree($tree);
        }
    }


    public function assign($id, $class)
    {
        $this->_classes[$id] = $class;
    }


    public function loadTree($data)
    {
        $this->_root = $this->createPermission(0, [
            'path'  => [],
            'id'    => 0,
            'title' => "Root",
            'value' => $data,
        ]);
    }


    public function getRoot()
    {
        return $this->_root;
    }


    /**
     * @param int   $type
     * @param array $options
     * @return mixed
     * @throws PermissionNotFoundException
     */
    public function createPermission($type, $options)
    {
        if (!isset($this->_classes[$type])) {
            throw new PermissionNotFoundException('Класс не обнаружен');
        }

        $class = $this->_classes[$type];
        return $this->_hashTable[implode('>', $options['path'])] = new $class($this, $options);
    }


    public function get($path)
    {
        if (!isset($this->_hashTable[$path])) {
            throw new \RuntimeException('Право не найдено');
        }

        return $this->_hashTable[$path];
    }
}
