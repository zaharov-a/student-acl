<?php


namespace Ox3a\Acl\Model\Permission;

/**
 * Class ArrayPermission
 * Право с выбираемым списком значений
 * @package Ox3a\Acl\Model\Permission
 */
class ArrayPermission extends AbstractPermission
{
    protected $_value   = [];
    protected $_options = [];


    public function setOptions($options)
    {
        $this->_options = $options;
    }


    public function getOptions()
    {
        return $this->_options;
    }


    public function setValue($value)
    {
        if (!is_array($value)) {
            throw new InvalidPermissionValueException('Неверное значение');
        }

        $this->_value = $value;
    }


    public function hasItem($item)
    {
        return in_array($item, $this->_value);
    }


}
