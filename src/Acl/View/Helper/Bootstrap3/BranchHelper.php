<?php


namespace Ox3a\Acl\View\Helper\Bootstrap3;


use Ox3a\Acl\Model\Permission\BranchPermission;

class BranchHelper extends AbstractHelper
{
    public function render()
    {
        /** @var BranchPermission $branch */
        $branch = $this->_model;

        $factory = Factory::getInstance();

        $branches = [];

        $items = [];
        foreach ($branch->getChildren() as $item) {
            if ($item->isBranch()) {
                $helper     = $factory->getHelper($item);
                $branches[] = $helper($item);
            } else {
                $helper  = $factory->getHelper($item);
                $items[] = $helper($item);
            }
        }

        if ($items) {
            $items = sprintf(
                '<table class="table table-condensed table-sm">
                            <thead>
                            <tr>
                                <th>Право</th>
                                <th>Значение</th>
                            </tr>
                            </thead>
                            <tbody>%s</tbody>
                        </table>',
                implode('', $items)
            );
        } else {
            $items = '';
        }

        if ($branches) {
            $branches = sprintf('<div class="panel-body">%s</div> ', implode('', $branches));
        } else {
            $branches = '';
        }

        return sprintf('
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">%s</h4>
    </div>
    %s
    %s
</div>
',
            $branch->getTitle(),
            $items,
            $branches
        );
    }


}
