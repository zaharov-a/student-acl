<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body>
<?php

use Ox3a\Acl\Model\Acl;
use Ox3a\Acl\Model\Permission\AccessPermission;
use Ox3a\Acl\View\Helper\Bootstrap3\BranchHelper;

require_once __DIR__ . '/../vendor/autoload.php';

$tree = [
    'folder1' => [
        'id'    => 0,
        'type'  => 0,
        'title' => '1',
        'value' => [
            'access'  => ['id' => 0, 'type' => 1, 'value' => null, 'title' => '2'],
            'access2' => ['id' => 0, 'type' => 1, 'value' => true, 'title' => '3'],
            'read'    => ['id' => 0, 'type' => 2, 'value' => AccessPermission::READ, 'title' => '4'],
            'write'   => ['id' => 0, 'type' => 2, 'value' => AccessPermission::WRITE, 'title' => '5'],
            'none'    => ['id' => 0, 'type' => 2, 'value' => AccessPermission::NONE, 'title' => '6'],
        ],
    ],
    'folder2' => [
        'id'    => 0,
        'type'  => 0,
        'title' => '7',
        'value' => [
            'access'   => ['id' => 0, 'type' => 1, 'value' => true, 'title' => '8'],
            'folder21' => [
                'id'    => 0,
                'type'  => 0,
                'title' => '9',
                'value' => [
                    'access'    => ['id' => 0, 'type' => 1, 'value' => false, 'title' => '10'],
                    'folder211' => [
                        'id'    => 0,
                        'type'  => 0,
                        'title' => '11',
                        'value' => [
                            'access' => ['id' => 0, 'type' => 1, 'value' => true, 'title' => '12'],
                        ],
                    ],
                ],
            ],
            'folder22' => [
                'id'    => 0,
                'type'  => 0,
                'title' => '',
                'value' => [],
            ],
        ],
    ],
    'folder3' => [
        'id'    => 0,
        'type'  => 0,
        'title' => '',
        'value' => [
            'access' => ['id' => 0, 'type' => 1, 'value' => false, 'title' => ''],
            'array'  => ['id' => 0, 'type' => 3, 'value' => null, 'title' => 'array', 'options' => [1 => 1, 2 => 2]],
        ],
    ],
];

$acl = new Acl($tree);

$viewHelper = new BranchHelper();

echo $viewHelper($acl->getRoot());
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Branch1</h4>
    </div>
    <div class="panel-body">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Branch11</h4>
            </div>
            <div class="panel-body">
                <div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Право</th>
                            <th>Значение</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Boolean</td>
                            <td>
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default active">
                                        <input type="radio" autocomplete="off" checked> Да
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" autocomplete="off"> Нет
                                    </label>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>Access</td>
                            <td>
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default active">
                                        <input type="radio" autocomplete="off" checked> нет
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" autocomplete="off"> чтение
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" autocomplete="off"> запись
                                    </label>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>Array</td>
                            <td>
                                <select class="form-control" multiple="multiple" size="5">
                                    <option>value 1</option>
                                    <option>value 2</option>
                                    <option>value 3</option>
                                    <option>value 4</option>
                                    <option>value 5</option>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Branch11</h4>
                    </div>
                    <div class="panel-body">
                        <div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Право</th>
                                    <th>Значение</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Boolean</td>
                                    <td>
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default active">
                                                <input type="radio" autocomplete="off" checked> Да
                                            </label>
                                            <label class="btn btn-default">
                                                <input type="radio" autocomplete="off"> Нет
                                            </label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Access</td>
                                    <td>
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default active">
                                                <input type="radio" autocomplete="off" checked> нет
                                            </label>
                                            <label class="btn btn-default">
                                                <input type="radio" autocomplete="off"> чтение
                                            </label>
                                            <label class="btn btn-default">
                                                <input type="radio" autocomplete="off"> запись
                                            </label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Array</td>
                                    <td>
                                        <select class="form-control" multiple="multiple" size="5">
                                            <option>value 1</option>
                                            <option>value 2</option>
                                            <option>value 3</option>
                                            <option>value 4</option>
                                            <option>value 5</option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Branch11</h4>
                            </div>
                            <div class="panel-body">
                                <div>
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Право</th>
                                            <th>Значение</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Boolean</td>
                                            <td>
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default active">
                                                        <input type="radio" autocomplete="off" checked> Да
                                                    </label>
                                                    <label class="btn btn-default">
                                                        <input type="radio" autocomplete="off"> Нет
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Access</td>
                                            <td>
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default active">
                                                        <input type="radio" autocomplete="off" checked> нет
                                                    </label>
                                                    <label class="btn btn-default">
                                                        <input type="radio" autocomplete="off"> чтение
                                                    </label>
                                                    <label class="btn btn-default">
                                                        <input type="radio" autocomplete="off"> запись
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Array</td>
                                            <td>
                                                <select class="form-control" multiple="multiple" size="5">
                                                    <option>value 1</option>
                                                    <option>value 2</option>
                                                    <option>value 3</option>
                                                    <option>value 4</option>
                                                    <option>value 5</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Branch12</h4>
            </div>
            <div class="panel-body">

            </div>
        </div>
    </div>
</div>
</body>
</html>
