<?php


namespace Ox3a\Acl\Exception;


use Exception;
use Throwable;

class AccessException extends Exception
{
    /**
     * Код по умолчанию - 403
     * AccessException constructor.
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 403, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
