<?php


namespace Ox3a\Acl\Model\Permission;

/**
 * Class BranchPermission
 * Ветвь прав
 * @package Ox3a\Acl\Model\Permission
 */
class BranchPermission extends AbstractPermission
{
    protected $_isBranch = true;

    /**
     * @var AbstractPermission[]
     */
    protected $_value = [];


    /**
     * Подумать: может value оставлять и дочерние элемент в children?
     * @param $value
     * @throws PermissionNotFoundException
     */
    public function setValue($value)
    {
        $this->_value = [];
        foreach ($value as $key => $permission) {
            $permission['path'] = array_merge($this->_path, [$key]);
            $this->_value[$key] = $this->_factory->createPermission($permission['type'], $permission);
        }
    }


    public function getValue()
    {
        $value = [];
        foreach ($this->_value as $key => $perm) {
            $value[$key] = [
                'type'  => $perm->getType(),
                'value' => $perm->getValue(),
            ];
        }

        return $value;
    }


    /**
     * @return AbstractPermission[]
     */
    public function getChildren()
    {
        return $this->_value;
    }


    /**
     * @param string $name
     * @return AbstractPermission
     * @throws PermissionNotFoundException
     */
    public function getChild($name)
    {
        if (!isset($this->_value[$name])) {
            throw new PermissionNotFoundException(
                sprintf("Право %s не найдено в ветке %s", $name, implode(">", $this->_path))
            );
        }
        return $this->_value[$name];
    }

}
