<?php


namespace Ox3a\Acl\View\Helper\Bootstrap3;


abstract class AbstractHelper
{
    protected $_view;

    /**
     * @var mixed
     */
    protected $_model;


    /**
     * @return string
     */
    abstract public function render();


    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->_model;
    }


    /**
     * @param mixed $element
     * @return AbstractHelper
     */
    public function setModel($element)
    {
        $this->_model = $element;
        return $this;
    }


    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }


    public function __toString()
    {
        return $this->render();
    }


    public function __invoke($element = null)
    {
        if ($element) {
            $this->setModel($element);
        }

        return $this;
    }


    public function getName()
    {
        return "permission[{$this->_model->getId()}]";
    }
}
