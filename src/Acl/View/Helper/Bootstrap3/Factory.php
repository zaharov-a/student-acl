<?php


namespace Ox3a\Acl\View\Helper\Bootstrap3;

use Ox3a\Acl\Model\Permission\AbstractPermission;
use ReflectionException;
use ReflectionClass;

class Factory
{
    /**
     * @var Factory
     */
    protected static $_instance;

    /**
     * Карта вьюшек по моделям
     * @var string[]
     */
    protected $_map = [];


    /**
     * @return Factory
     */
    public static function getInstance()
    {
        return self::$_instance ?: (self::$_instance = new self());
    }


    /**
     * @param AbstractPermission $model
     * @return AbstractHelper
     * @throws ReflectionException
     */
    public static function factory($model)
    {
        return self::getInstance()->getHelper($model);
    }


    /**
     * @param AbstractPermission $model
     * @return AbstractHelper
     * @throws ReflectionException
     */
    public function getHelper($model)
    {
        $reflection = new ReflectionClass($model);
        if (!isset($this->_map[$reflection->getName()])) {
            $this->assignHelper($reflection->getName(), 'Ox3a\\Acl\\View\\Helper\\Bootstrap3\\' . str_replace('Permission', 'Helper', (new ReflectionClass($model))->getShortName()));
        }

        $class = $this->_map[$reflection->getName()];

        return new $class();
    }


    /**
     * Ассоциировать класс модели с классом хелпера
     * @param $elementClassName
     * @param $helperClassName
     */
    public function assignHelper($elementClassName, $helperClassName)
    {
        $this->_map[$elementClassName] = $helperClassName;
    }
}

