<?php


namespace Ox3a\Acl\View\Helper\Bootstrap3;


use Ox3a\Acl\Model\Permission\BooleanPermission;

class BooleanHelper extends AbstractHelper
{
    protected $_list = [
        ['value' => 0, 'title' => 'Нет'],
        ['value' => 1, 'title' => 'Да'],
    ];


    public function render()
    {
        /** @var BooleanPermission $permission */
        $permission = $this->_model;

        $control = '';
        $name    = $this->getName();

        foreach ($this->_list as $item) {
            $check   = $item['value'] == $permission->getValue();
            $control .= sprintf('<label class="btn btn-default %s">
                                        <input type="radio" autocomplete="off" name="%s" value="%s" %s> %s
                                    </label>',
                $check ? 'active' : '',
                $name,
                $item['value'],
                $check ? 'checked' : '',
                $item['title']);
        }
        $control = "<div class=\"btn-group btn-group-sm\" data-toggle=\"buttons\">{$control}</div>";
        return sprintf(
            '<tr><td>%s</td><td>%s</td></tr>',
            $permission->getTitle(),
            $control
        );
    }

}
