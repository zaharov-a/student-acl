<?php


namespace Ox3a\Acl\Model\Permission;


/**
 * Class AccessPermission
 * Право на доступ: нет, только чтение, полный
 * @package Ox3a\Acl\Model\Permission
 */
class AccessPermission extends AbstractPermission
{
    const NONE  = 0b0;
    const READ  = 0b1;
    const WRITE = 0b11;

    protected $_value = 0;


    public function setValue($value)
    {
        $list = [
            self::NONE,
            self::READ,
            self::WRITE,
        ];

        if (!in_array($value, $list)) {
            throw new InvalidPermissionValueException('Неверное значение');
        }
        $this->_value = (int)$value;
    }


    public function isWritable()
    {
        return $this->_value === self::WRITE;
    }


    public function isReadable()
    {
        return $this->_value & self::READ;
    }


    public function isDenied()
    {
        return $this->_value === self::NONE;
    }


}
