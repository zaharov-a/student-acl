<?php

use PHPUnit\Framework\TestCase;
use Ox3a\Acl\Model\Permission\AccessPermission;
use Ox3a\Acl\Model\Acl;

class AclTest extends TestCase
{

    /**
     * @param $tree
     * @param $path
     * @param $expected
     * @dataProvider dataProvider1
     */
    public function test1($tree, $path, $expected)
    {
        $acl = new Acl($tree);

        $this->assertEquals($acl->get($path)->getValue(), $expected);
    }


    /**
     * @param $tree
     * @param $path
     * @param $expected
     * @dataProvider dataProvider2
     */
    public function test2($tree, $path, $expected)
    {
        $acl = new Acl($tree);
        $this->assertEquals(json_encode($acl->get($path)), json_encode($expected));
    }


    public function getTree1()
    {
        return [
            'folder1' => [
                'id'    => 0,
                'type'  => 0,
                'title' => '',
                'value' => [
                    'access'  => ['id' => 0, 'type' => 1, 'value' => null, 'title'=>''],
                    'access2' => ['id' => 0, 'type' => 1, 'value' => true, 'title'=>''],
                    'read'    => ['id' => 0, 'type' => 2, 'value' => AccessPermission::READ, 'title'=>''],
                    'write'   => ['id' => 0, 'type' => 2, 'value' => AccessPermission::WRITE, 'title'=>''],
                    'none'    => ['id' => 0, 'type' => 2, 'value' => AccessPermission::NONE, 'title'=>''],
                ],
            ],
            'folder2' => [
                'id'    => 0,
                'type'  => 0,
                'title' => '',
                'value' => [
                    'access'   => ['id' => 0, 'type' => 1, 'value' => true, 'title'=>''],
                    'folder21' => [
                        'id'    => 0,
                        'type'  => 0,
                        'title' => '',
                        'value' => [
                            'access'    => ['id' => 0, 'type' => 1, 'value' => false, 'title'=>''],
                            'folder211' => [
                                'id'    => 0,
                                'type'  => 0,
                                'title' => '',
                                'value' => [
                                    'access' => ['id' => 0, 'type' => 1, 'value' => true, 'title'=>''],
                                ],
                            ],
                        ],
                    ],
                    'folder22' => [
                        'id'    => 0,
                        'type'  => 0,
                        'title' => '',
                        'value' => [],
                    ],
                ],
            ],
            'folder3' => [
                'id'    => 0,
                'type'  => 0,
                'title' => '',
                'value' => [
                    'access' => ['id' => 0, 'type' => 1, 'value' => false, 'title'=>''],
                ],
            ],
        ];
    }


    public function dataProvider1()
    {
        $tree = $this->getTree1();

        return [
            [$tree, 'folder1>access', false],
            [$tree, 'folder1>access2', true],
            [$tree, 'folder1>read', AccessPermission::READ],
            [$tree, 'folder1>write', AccessPermission::WRITE],
            [$tree, 'folder1>none', AccessPermission::NONE],
            [$tree, 'folder2>folder21>folder211>access', true],
        ];
    }


    public function dataProvider2()
    {
        $tree = $this->getTree1();

        return [
            [$tree, 'folder1>access', false],
            [$tree, 'folder1>access2', true],
            [$tree, 'folder1>read', AccessPermission::READ],
            [$tree, 'folder1>write', AccessPermission::WRITE],
            [$tree, 'folder1>none', AccessPermission::NONE],
            [$tree, 'folder2>folder21>folder211>access', true],
            [$tree, 'folder1', [
                'access'  => false,
                'access2' => true,
                'read'    => AccessPermission::READ,
                'write'   => AccessPermission::WRITE,
                'none'    => AccessPermission::NONE,
            ]],
            [$tree, 'folder2', [
                'access'   => true,
                'folder21' => [
                    'access'    => false,
                    'folder211' => [
                        'access' => true,
                    ],
                ],
                'folder22' => [],
            ]],
        ];
    }

}
